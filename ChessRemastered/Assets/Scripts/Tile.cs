﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour 
{
	[HideInInspector]
	public Vector3 position = Vector3.zero;
	public Vector2 size = Vector2.zero;
	public bool isWhite = false;
	public Vector2 tileID;
	public bool isOccupied = false;
	public GameObject pieceOccupied;
	
	public void setPosition(Vector3 position)
	{
		this.position = position;
		transform.position = position;
	}
	
	public void setColour(bool white)
	{
		Renderer tileRend = GetComponent<Renderer>();
		if (tileRend != null)
		{
			if (white)
			{
				tileRend.material.color = Color.white;	
			}
			else
			{
				tileRend.material.color = Color.black;
			}
		}
	}
	
	public void setTileID(Vector2 tileID)
	{
		this.tileID = tileID;
	}
	
	public void setIsOccupied(bool state, bool isWhite, GameObject piece)
	{
		isOccupied = state;
		pieceOccupied = piece;
		this.isWhite = isWhite;
	}
}