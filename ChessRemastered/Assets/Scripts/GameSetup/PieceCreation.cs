using UnityEngine;
using System.Collections;

[System.Serializable]
public struct ChessPieceStats
{
	public GameObject piecePrefab;
	public float yPosition;
}

public enum CHESS_TYPE
{
	Pawn,
	Bishop,
	Knight,
	Rook,
	Queen,
	King,
	None
};

public class PieceCreation : MonoBehaviour 
{
	public ChessPieceStats pawn, bishop, knight, rook, queen, king;
	
	public void createChessPiece(int tileCount, Vector3 tilePosition, Vector2 tileID)
	{
		//determine type of chess piece
		CHESS_TYPE ENUM_CHESS_TYPE = checkPieceType(tileCount);
		//spawn the type of chess piece
		GameObject chessPiece;
		chessPiece = spawnPiece(ENUM_CHESS_TYPE, tilePosition);
		if (chessPiece == null)
			return;
		//determine the team the piece belongs to
		setPieceTeam(tileCount, chessPiece, tileID);
		//set position of the piece
	}
	
	private GameObject spawnPiece(CHESS_TYPE ENUM_CHESS_TYPE, Vector3 spawnPosition)
	{
		GameObject chessPiece;
		Vector3 tilePosition = Vector3.zero;
		tilePosition.z = spawnPosition.z;
		tilePosition.x = spawnPosition.x;
		switch(ENUM_CHESS_TYPE)
		{
		case CHESS_TYPE.Pawn:
			chessPiece = (GameObject)Instantiate(pawn.piecePrefab);
			tilePosition.y = pawn.yPosition;
			break;
		case CHESS_TYPE.Rook:
			chessPiece = (GameObject)Instantiate(rook.piecePrefab);
			tilePosition.y = rook.yPosition;
			break;
		case CHESS_TYPE.Knight:
			chessPiece = (GameObject)Instantiate(knight.piecePrefab);
			tilePosition.y = knight.yPosition;
			break;
		case CHESS_TYPE.Bishop:
			chessPiece = (GameObject)Instantiate(bishop.piecePrefab);
			tilePosition.y = bishop.yPosition;
			break;
		case CHESS_TYPE.Queen:
			chessPiece = (GameObject)Instantiate(queen.piecePrefab);
			tilePosition.y = queen.yPosition;
			break;
		case CHESS_TYPE.King:
			chessPiece = (GameObject)Instantiate(king.piecePrefab);
			tilePosition.y = king.yPosition;
			break;
		default:
			chessPiece = null;
			break;
		}
		if (chessPiece != null)
		{
			chessPiece.transform.position = tilePosition;
		}
		return chessPiece;
	}
	
	public CHESS_TYPE checkPieceType(int tileCount)
	{
		if ((tileCount >= 9 && tileCount <= 16) || (tileCount >= 49 && tileCount <= 56))
		{
			//pawn
			return CHESS_TYPE.Pawn;
		}
		else if (tileCount == 1 || tileCount == 8 || tileCount == 57 || tileCount == 64)
		{
			//rook
			return CHESS_TYPE.Rook;
		}
		else if (tileCount == 2 || tileCount == 7 || tileCount == 63 || tileCount == 58)
		{
			//knight
			return CHESS_TYPE.Knight;
		}
		else if (tileCount == 3 || tileCount == 6 || tileCount == 59 || tileCount == 62)
		{
			//bishop
			return CHESS_TYPE.Bishop;
		}
		else if (tileCount == 4 || tileCount == 60)
		{
			//queen
			return CHESS_TYPE.Queen;
		}
		else if (tileCount == 5 || tileCount == 61)
		{
			//king
			return CHESS_TYPE.King;
		}
		return CHESS_TYPE.None;
	}

	public void setPieceTeam(int tileCount, GameObject piece, Vector2 tileID)
	{
		Renderer pieceRend = piece.GetComponent<Renderer>();
		Piece chessPieceComponent = piece.GetComponent<Piece>();
		if (pieceRend != null)
		{
			if (tileCount <= 16)
			{
				//set white chess team
				pieceRend.material.color = Color.white;
				if (chessPieceComponent != null)
				{
					chessPieceComponent.isWhite = true;
				}
			}
			else
			{
				//set black chess team
				pieceRend.material.color = Color.black;
				if (chessPieceComponent != null)
				{
					chessPieceComponent.isWhite = false;
				}
			}
		}
		
		if (chessPieceComponent != null)
		{
			chessPieceComponent.setPiecePosition(tileID);
		}
	}
}
