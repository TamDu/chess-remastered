﻿using UnityEngine;
using System.Collections;

public class BoardCreation : MonoBehaviour 
{
	public Vector2 offsets = Vector2.zero;
	//grid sizes
	public int gridX = 8;
	public int gridZ = 8;
	public GameObject tilePrefab;
	public PieceCreation pieceCreation;
	private GameController gameCtrl;
	
	private void Start()
	{
		gameCtrl = GetComponent<GameController>();
		for (int z = 0; z < gridZ; z++)
		{
			for (int x = 0; x < gridX; x++)
			{
				createTile(x, z);
			}
		}
	}
	
	public void createTile(int x, int z)
	{
		//create tile
		GameObject tileGO = (GameObject)Instantiate(tilePrefab);
		Vector3 spawnPosition = Vector3.zero;
		Vector2 tileID = new Vector2(x, z);
		Tile tile = tileGO.GetComponent<Tile>();
		if (tile != null)
		{
			//place tile in world
			spawnPosition.z =  z * tile.size.y;
			spawnPosition.x =  x * tile.size.x;
			tile.setPosition(spawnPosition);
			//set colour
			bool isWhite = checkColour(x, z);
			//set tile colour
			tile.setColour(isWhite);
			//set tile id
			tile.setTileID(tileID);
			gameCtrl.tiles.Add(tile);
		}
		
		int tileCount = 0;
		tileCount = calculatetileCount(x, z);
		bool canPlacePiece = false;
		canPlacePiece = checkStartingTile(tileCount);
		if (canPlacePiece)
		{
			pieceCreation.createChessPiece(tileCount, spawnPosition, tile.tileID);
		}
	}
	
	//checks if the tile is white coloured tile
	public bool checkColour(int x, int z)
	{
//		int tileCount = 0;
//		tileCount = calculatetileCount(x, z);
		if (z % 2 == 0)
		{
			if ((x + 1) % 2 == 0)
			{
				return true;
			}
		}
		else if ((x + 1) % 2 != 0)
		{
			return true;
		}
		return false;
	}
	
	//checks if a piece will be placed on the tile.
	public bool checkStartingTile(int tileCount)
	{
		if (tileCount <= 16 || tileCount >= 33)
		{
			return true;
		}
		return false;
	}
	
	public int calculatetileCount(int x, int z)
	{
		int tileCount = 0;
		tileCount = (8 * z) + (x + 1);
		return tileCount;
	}
}
