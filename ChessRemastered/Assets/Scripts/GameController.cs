﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour 
{
	public Text winText = null;
	public List<Tile> tiles;
	public bool isTurnWhite = true;
	public LayerMask layer;
	public float rayLength = 200f;
	public bool isProcessingTurn = false;
	public Transform whiteCameraPositions;
	public Transform blackCameraPositions;
	private GameObject currentSelectedPiece;
	
	public bool checkIsWhite(Vector2 tileID)
	{
		for (int i = 0; tiles.Count < i; i++)
		{
			if (tiles[i].tileID == tileID)
			{
				return true;
			}
		}
		return false;
	}
	
	private void Update()
	{
		debugControls();
	}
	
	private void debugControls()
	{
		if (Input.GetMouseButtonDown(0))
		{
			if (isProcessingTurn == false)
				mouseRayCast();
		}
	}
	
	public void mouseRayCast()
	{
		Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit rayHit;
		if (Physics.Raycast(camRay, out rayHit, rayLength, layer))
		{
			Tile tile = rayHit.collider.gameObject.GetComponent<Tile>();
			if (tile != null)
			{
				controls(tile);
			}
		}
	}
	
	private void controls(Tile tile)
	{
		if (tile.isOccupied && isTurnWhite == tile.isWhite)
		{
			//select piece
			if (tile.pieceOccupied != currentSelectedPiece)
			{
				deselectPiece();
				selectPiece(tile);
			}
			else if (currentSelectedPiece == null)
			{
				selectPiece(tile);
			}
			else if (currentSelectedPiece != null)
			{
				deselectPiece();
			}
		}
		else if (currentSelectedPiece != null)
		{
			moveSelectedPiece(tile);
		}
	}
	
	private void selectPiece(Tile tile)
	{
		currentSelectedPiece = tile.pieceOccupied;
		tileIndicator();
		changeColour(Color.blue);
	}
	
	public void endTurn()
	{
		isTurnWhite = !isTurnWhite;
		if (isTurnWhite)
		{
			Camera.main.transform.position = whiteCameraPositions.position;
			Camera.main.transform.rotation = whiteCameraPositions.rotation;
		}
		else
		{
			Camera.main.transform.position = blackCameraPositions.position;
			Camera.main.transform.rotation = blackCameraPositions.rotation;
		}
	}
	
	public void deselectPiece()
	{
		if (currentSelectedPiece != null)
		{
			if(currentSelectedPiece.GetComponent<Piece>().isWhite)
			{
				changeColour(Color.white);
			}
			else
			{
				changeColour(Color.black);
			}
			currentSelectedPiece = null;
			resetTileColour();
		}
	}
	
	private void moveSelectedPiece(Tile tile)
	{
		//move
		Piece chessPiece = currentSelectedPiece.GetComponent<Piece>();
		if (chessPiece != null)
		{
			chessPiece.moveChessPiece(tile.tileID);
		}
	}
	
	private void changeColour(Color colour)
	{
		currentSelectedPiece.GetComponent<Renderer>().material.color = colour;
	}
	
	//highlgihts where pieces can move
	private void tileIndicator()
	{
		currentSelectedPiece.GetComponent<ChessPiece>().checkAvailableTiles();
	}
	
	private void resetTileColour()
	{
		for (int i = 0; i < tiles.Count; i++)
		{
			if (checkColour(tiles[i].tileID.x, tiles[i].tileID.y))
			{
				tiles[i].setColour(true);
			}
			else
			{
				tiles[i].setColour(false);
			}
		}
	}
	
	//checks if the tile is white
	public bool checkColour(float x, float z)
	{
		//		int tileCount = 0;
		//		tileCount = calculatetileCount(x, z);
		if (z % 2 == 0)
		{
			if ((x + 1) % 2 == 0)
			{
				return true;
			}
		}
		else if ((x + 1) % 2 != 0)
		{
			return true;
		}
		return false;
	}
	
	public void displayWinner(bool isWhiteWinner)
	{
		StartCoroutine(restartGame());

		if (!isWhiteWinner)
		{
			//display if white has won conditions
			winText.text = "White wins";
			print("White has won");
		}
		else
		{
			//display if black has won
			winText.text = "Black wins";
			print("Black has won");
		}
	}

	private IEnumerator restartGame()
	{
		yield return new WaitForSeconds(5f);
		Application.LoadLevel(Application.loadedLevel);
	}

}
