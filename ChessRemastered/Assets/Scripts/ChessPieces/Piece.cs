using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Piece : MonoBehaviour 
{
	public GameController gameCtrl;
	public bool isWhite = false;
	public float speed = 150;
	private Vector3 previousPosition;
	private float distRatio = 0;
	protected Vector2 currentTileID;
//	protected bool isMoving = false;
	
	protected virtual void Awake()
	{
		if (gameCtrl == null)
			gameCtrl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
	}
	
	protected virtual void Start()
	{
		previousPosition = transform.position;
	}
	
	public bool checkIsOccupied(Vector2 tileID)
	{
		Tile tile = null;
		tile = getTile(tileID);
		if (tile != null)
		{
			if (tile.isOccupied)
			{
				return true;
			}
		}
		return false;
	}
	
	public bool checkIsEnemy(Vector2 tileID)
	{
		Tile tile = null;
		tile = getTile(tileID);
		//checks if is enemy
		if (tile != null)
		{
			if (tile.isWhite != isWhite)
			{
				return true;
			}
		}
		return false;
	}
	
	public virtual void moveChessPiece(Vector2 tileID)
	{
	
	}
	
	protected void move(Vector3 position)
	{
		StartCoroutine(timedMovement(position));
	}
	
	private IEnumerator timedMovement(Vector3 position)
	{
	//	isMoving = true;
		gameCtrl.isProcessingTurn = true;
		position.y = transform.position.y;
		while(distRatio <= 1)
		{
			distRatio += Time.deltaTime * (speed/100);
			transform.position = Vector3.Lerp(previousPosition, position, distRatio);
			yield return null;
		}
		previousPosition = position;
		distRatio = 0;
		gameCtrl.isProcessingTurn = false;
		gameCtrl.endTurn();
//		isMoving = false;
	}
	
	public void setPiecePosition(Vector2 tileID)
	{
		Tile tile = null;
		tile = getTile(tileID);
		if (tile != null)
		{
			tile.setIsOccupied(true, isWhite, gameObject);
			currentTileID = tile.tileID;
		}
	}
	
	protected void resetPosition(Vector2 tileID)
	{
		removePiecePosition(currentTileID);
		setPiecePosition(tileID);
		gameCtrl.deselectPiece();
	}
	
	public virtual void removePiece()
	{
		removePiecePosition(currentTileID);
		Destroy(gameObject);
	}
	
	protected void removePiecePosition(Vector2 tileID)
	{
		Tile tile = null;
		tile = getTile(tileID);
		if (tile != null)
		{
			tile.setIsOccupied(false, isWhite, null);
		}
	}
	
	protected Tile getTile(Vector2 tileID)
	{
		for (int i = 0; i < gameCtrl.tiles.Count; i++)
		{
			if (gameCtrl.tiles[i].tileID == tileID)
			{
				return gameCtrl.tiles[i]; 
			}
		}
		return null;
	}
}
