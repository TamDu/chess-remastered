﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bishop : ChessPiece 
{
	public override List<Vector2> checkAvailableTiles ()
	{
		List<Vector2> availableTiles = new List<Vector2>();
		//top left tiles
		Vector2 tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -1, 1, tileChecker );
		
		//top right tiles
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 1, 1, tileChecker );
		
		//bottom left tiles
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -1, -1, tileChecker );
		
		//bottom right tiles
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 1, -1, tileChecker );
		return availableTiles;
	}
}
