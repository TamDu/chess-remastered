﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class King : ChessPiece 
{
	public override List<Vector2> checkAvailableTiles ()
	{
		List<Vector2> availableTiles = new List<Vector2>();
		//checks forward
		Vector2 tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 0, 1, tileChecker );
		
		//checks backwards
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 0, -1, tileChecker );
		
		//checks right
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 1, 0, tileChecker );
		
		//checks left
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -1, 0, tileChecker );
		
		//top left tiles
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -1, 1, tileChecker );
		
		//top right tiles
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 1, 1, tileChecker );
		
		//bottom left tiles
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -1, -1, tileChecker );
		
		//bottom right tiles
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 1, -1, tileChecker );
		return availableTiles;
	}
	
	protected override List<Vector2> availableTileIncremter(List<Vector2> availableTiles, float xIncrements, float yIncrements, Vector2 tileChecker)
	{
		tileChecker.y += yIncrements;
		tileChecker.x += xIncrements;
		if (tileChecker.y >= 9 || tileChecker.y <= -1 || tileChecker.x >= 9 || tileChecker.x <= -1)
		{
			return availableTiles;
		}
		if (checkIsOccupied(tileChecker) == false)
		{
			Tile tile = getTile(tileChecker);
			if (tile != null)	
				tile.GetComponent<Renderer>().material.color = Color.green;
			availableTiles.Add(tileChecker);
		}
		else if (checkIsEnemy(tileChecker) == true)
		{
			Tile tile = getTile(tileChecker);
			if (tile != null)
				tile.GetComponent<Renderer>().material.color = Color.red;
			availableTiles.Add(tileChecker);
		}
		return availableTiles;
	}
	
	public override void removePiece ()
	{
		gameCtrl.displayWinner(isWhite);
		base.removePiece ();
	}
}
