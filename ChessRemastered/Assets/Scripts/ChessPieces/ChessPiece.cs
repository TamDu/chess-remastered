﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//can move forward twice on the first time these piece moves
//moves forward once after
//attacks diagonally
public class ChessPiece : Piece 
{
	protected bool hasMoved = false;

	private void debugControls()
	{
	
	}

	public override void moveChessPiece(Vector2 tileID)
	{
		if (isAvailable(tileID))
		{
			Tile tile = null;
			tile = getTile(tileID);
			if (tile != null)
			{
				if (tile.pieceOccupied != null)
				{
					tile.pieceOccupied.GetComponent<Piece>().removePiece();
				}
				move(tile.position);		
			}
			else
			{
				Debug.LogWarning("Tile selected is null");
			}
			resetPosition(tileID);
			endOfMove();
		}
	}
	
	public virtual void endOfMove()
	{
		
	}
	
	private bool isAvailable(Vector2 tileID)
	{
		List<Vector2> availableTiles = checkAvailableTiles();
		for (int i = 0; i < availableTiles.Count; i++)
		{
			if (availableTiles[i] == tileID)
				return true;
		}
		return false;
	}
	
	public virtual List<Vector2> checkAvailableTiles()
	{
		List<Vector2> availableTiles = new List<Vector2>();
		return availableTiles;
	}
	
	protected virtual List<Vector2> availableTileIncremter(List<Vector2> availableTiles, float xIncrements, float yIncrements, Vector2 tileChecker)
	{
		for (int i = 0; i < 8; i++)
		{
			tileChecker.y += yIncrements;
			tileChecker.x += xIncrements;
			if (tileChecker.y >= 9 || tileChecker.y <= -1 || tileChecker.x >= 9 || tileChecker.x <= -1)
			{
				break;
			}
			if (checkIsOccupied(tileChecker) == false)
			{
				Tile tile = getTile(tileChecker);
				if (tile != null)
					tile.GetComponent<Renderer>().material.color = Color.green;
				availableTiles.Add(tileChecker);
			}
			else if (checkIsEnemy(tileChecker) == true)
			{
				Tile tile = getTile(tileChecker);
				if (tile != null)
					tile.GetComponent<Renderer>().material.color = Color.red;
				availableTiles.Add(tileChecker);
				break;
			}
			else
			{
				break;
			}
		}
		return availableTiles;
	}
}
