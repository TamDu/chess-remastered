﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Queen : ChessPiece 
{
	public override List<Vector2> checkAvailableTiles ()
	{
		List<Vector2> availableTiles = new List<Vector2>();
		//checks forward
		Vector2 tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 0, 1, tileChecker );
		
		//checks backwards
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 0, -1, tileChecker );
		
		//checks right
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 1, 0, tileChecker );
		
		//checks left
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -1, 0, tileChecker );
		
		//top left tiles
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -1, 1, tileChecker );
		
		//top right tiles
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 1, 1, tileChecker );
		
		//bottom left tiles
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -1, -1, tileChecker );
		
		//bottom right tiles
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 1, -1, tileChecker );
		return availableTiles;
	}
}
