﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Pawn : ChessPiece
{

	public override List<Vector2> checkAvailableTiles ()
	{
		List<Vector2> availableTiles = new List<Vector2>();
		Vector2 tileChecker = currentTileID;
		
		if (isWhite)
		{
			//checks forward
			for (int i = 0; i < 2; i++)
			{
				if (hasMoved && i == 1)
				{
					break;
				}
				tileChecker.y++;
				if (checkIsOccupied(tileChecker) == false)
				{
					Tile tile = getTile(tileChecker);
					if (tile != null)	
						tile.GetComponent<Renderer>().material.color = Color.green;
					availableTiles.Add(tileChecker);
				}
				else
				{
					break;
				}
			}
			
			//checks diangonally
			for (int i = 0; i < 2; i++)
			{
				tileChecker = currentTileID;
				tileChecker.y++;
				if (i == 1)
					tileChecker.x++;
				else
					tileChecker.x--;
				if (checkIsOccupied(tileChecker) == true && checkIsEnemy(tileChecker) == true)
				{
					Tile tile = getTile(tileChecker);
					if (tile != null)	
						tile.GetComponent<Renderer>().material.color = Color.red;
					availableTiles.Add(tileChecker);
				}
			}
		}
		else
		{
			//checks forward
			for (int i = 0; i < 2; i++)
			{
				if (hasMoved && i == 1)
				{
					break;
				}
				tileChecker.y--;
				if (checkIsOccupied(tileChecker) == false)
				{
					Tile tile = getTile(tileChecker);
					if (tile != null)	
						tile.GetComponent<Renderer>().material.color = Color.green;
					availableTiles.Add(tileChecker);
				}
				else
				{
					break;
				}
			}
			
			//checks diangonally
			for (int i = 0; i < 2; i++)
			{
				tileChecker = currentTileID;
				tileChecker.y--;
				if (i == 1)
					tileChecker.x++;
				else
					tileChecker.x--;
				if (checkIsOccupied(tileChecker) == true && checkIsEnemy(tileChecker) == true)
				{
					Tile tile = getTile(tileChecker);
					if (tile != null)	
						tile.GetComponent<Renderer>().material.color = Color.red;
					availableTiles.Add(tileChecker);
				}
			}
		}
		return availableTiles;
	}
	
	public override void endOfMove ()
	{
		hasMoved = true;
	}
}
