﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rook : ChessPiece 
{
	public override List<Vector2> checkAvailableTiles ()
	{
		List<Vector2> availableTiles = new List<Vector2>();
		//checks forward
		Vector2 tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 0, 1, tileChecker );
		
		//checks backwards
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 0, -1, tileChecker );
		
		//checks right
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 1, 0, tileChecker );
		
		//checks left
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -1, 0, tileChecker );
		return availableTiles;
	}
	
	public override void endOfMove ()
	{
		hasMoved = true;
	}
}
