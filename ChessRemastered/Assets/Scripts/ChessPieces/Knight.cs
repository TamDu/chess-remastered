﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Knight : ChessPiece 
{
	public override List<Vector2> checkAvailableTiles ()
	{
		List<Vector2> availableTiles = new List<Vector2>();
		//checks 3 forward one left
		Vector2 tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -1, 2, tileChecker );
		
		//checks 3 forward one right
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 1, 2, tileChecker );
		
		//checks 3 right one forward
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 2, 1, tileChecker );
		
		//checks 3 right one backwards
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 2, -1, tileChecker );
		
		//checks 3 left one forward
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -2, 1, tileChecker );
		
		//checks 3 left one backwards
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -2, -1, tileChecker );
	
		//checks 3 backwards one left
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, -1, -2, tileChecker );
	
		//checks 3 backwards one right
		tileChecker = currentTileID;
		availableTiles = availableTileIncremter(availableTiles, 1, -2, tileChecker );
	
		return availableTiles;
	}
	
	protected override List<Vector2> availableTileIncremter(List<Vector2> availableTiles, float xIncrements, float yIncrements, Vector2 tileChecker)
	{
		tileChecker.y += yIncrements;
		tileChecker.x += xIncrements;
		if (tileChecker.y >= 9 || tileChecker.y <= -1 || tileChecker.x >= 9 || tileChecker.x <= -1)
		{
			return availableTiles;
		}
		if (checkIsOccupied(tileChecker) == false)
		{
			Tile tile = getTile(tileChecker);
			if (tile != null)
				tile.GetComponent<Renderer>().material.color = Color.green;
			availableTiles.Add(tileChecker);
		}
		else if (checkIsEnemy(tileChecker) == true)
		{
			Tile tile = getTile(tileChecker);
			if (tile != null)	
				tile.GetComponent<Renderer>().material.color = Color.red;
			availableTiles.Add(tileChecker);
		}
		return availableTiles;
	}
}
